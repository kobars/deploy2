from flask import Flask
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)

data = [{"client_id": 1,"client_key": "CLIENT01","client_secret": "SECRET01","status": True},{"client_id": 2,"client_key": "CLIENT02","client_secret": "SECRET01","status": False},{"client_id": 3,"client_key": "CLIENT03","client_secret": "SECRET03","status": False},{"client_id": 4,"client_key": "CLIENT04","client_secret": "SECRET04","status": False}]

class Client(Resource):
    def get(self, client_id = None):
        if client_id == None:
            return data, 200
        else:
            for i in data:
                if i['client_id'] == client_id:
                    return i
            return {'message':'Not_Found'},404
            
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, help='Client ID is required, cannot be blanked', location='json',required=True)
        parser.add_argument('client_key', type=str, help='Client Key is required, cannot be blanked', location='json',required=True)
        parser.add_argument('client_secret', type=str, help='Client Secret is required, cannot be blanked', location='json',required=True)
        parser.add_argument('status', type=bool, help='', location='json',required=False) #Status tidak termasuk syarat required
        args = parser.parse_args()
        for i in data:
            if i["client_id"] == args["client_id"]:
                return {"message" : "Client id has been exist"},444

        data.append(args)
        return data, 201
    
    def put(self, client_id):
        parser = reqparse.RequestParser()
        parser.add_argument('client_id', type=int, help='Client ID is required, cannot be blanked', location='json', required=True)
        parser.add_argument('client_key', type=str, help='', location='json', required=True)
        parser.add_argument('client_secret', type=str, help='', location='json', required=True)
        parser.add_argument('status', type=bool, help='', location='json', required=True)
        args = parser.parse_args()

        if client_id == args['client_id']: #Kondisi jika user tidak merubah ID
            for i in data:
                if i['client_id'] == args['client_id']:
                    i["client_secret"] = args["client_secret"]
                    i["client_key"] = args["client_key"]
                    i["status"] = args["status"]
                    return i
        else:
            return "id cannot be changed" #Kondisi jika user merubah ID

        return {"message": "Not Found"}, 404
    
    def delete(self, client_id):
        for i in data:
            if i["client_id"] == client_id:
                data.remove(i)
                return data
            
        return {'message':'DELETED'}, 200



api.add_resource(Client,'/clients','/client/<int:client_id>')

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1',port =5000)

